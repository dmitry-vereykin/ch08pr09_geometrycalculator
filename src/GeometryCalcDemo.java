/**
 * Created by Dmitry Vereykin on 7/28/2015.
 */
import java.util.Scanner;

public class GeometryCalcDemo {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int figure;

        while (true) {

            System.out.println("\nGeometry Calculator");
            System.out.println("1. Calculate th Area of a Circle");
            System.out.println("2. Calculate th Area of a Rectangle");
            System.out.println("3. Calculate th Area of a Triangle");
            System.out.println("4. Quit");
            System.out.print("\nEnter your choice (1-4): ");
            figure = keyboard.nextInt();

            if (figure == 1) {
                System.out.print("What is the radius of the circle? ");
                double radius = keyboard.nextDouble();
                if (check(radius)) continue;
                double area = GeometryCalc.getArea(radius);
                System.out.println("The area of the circle is " + area + ".");
            } else if (figure == 2) {
                System.out.print("What is the length of the rectangle? ");
                double length = keyboard.nextDouble();
                if (check(length)) continue;
                System.out.print("What is the width of the rectangle? ");
                double width = keyboard.nextDouble();
                if (check(width)) continue;
                double area = GeometryCalc.getArea(length, width);
                System.out.println("The area of the rectangle is " + area + ".");
            } else if (figure == 3) {
                System.out.print("What is the base of the triangle? ");
                double base = keyboard.nextDouble();
                if (check(base)) continue;
                System.out.print("What is the height of the triangle ");
                double height = keyboard.nextDouble();
                if (check(height)) continue;
                double constant = 0.5;
                double area = GeometryCalc.getArea(base, height, constant);
                System.out.println("The area of the triangle is " + area + ".");
            } else if (figure == 4) {
                break;
            } else {
                System.out.println("\nWrong input, only 1 - 4");
                continue;
            }

        }
        System.out.println("Program is ended as requested");
    }

    public static boolean check(double number) {
        if (number < 0) {
            System.out.println("\nCan't be negative.");
            return true;
        }
        return false;
    }
}